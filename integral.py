import math

def integrate_abs_sin(lower, upper, N):
    delta_x = (upper - lower) / N
    integral_sum = 0.0

    for i in range(N):
        x_i = lower + i * delta_x
        integral_sum += abs(math.sin(x_i)) * delta_x

    return integral_sum

def main():
    interval_lower = float(input("Enter lower bound: "))
    interval_upper = float(input("Enter upper bound: "))
    N_values = [10, 100, 1000, 10000, 100000, 1000000]

    print("Numerical Integration Results:")
    for N in N_values:
        result = integrate_abs_sin(interval_lower, interval_upper, N)
        print(f"N = {N}: {result}")

if __name__ == "__main__":
    main()
