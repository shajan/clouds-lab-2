import azure.functions as func
import azure.durable_functions as df 
import os

def hello_orchestrator(context: df.DurableOrchestrationContext):

    connectstr = "DefaultEndpointsProtocol=https;AccountName=annettemrwordcount;AccountKey=mzjIzhBLccXlosHtEAg2roM77t0cUr/+8FT/9WWxeVor7AqXotfiS9Gt/7hpkZKSIwcAKAeGVyux+ASttA0BXQ==;EndpointSuffix=core.windows.net"
    # print(connectstr)
    files= yield context.call_activity("E1_GetInputDataFn", connectstr)
    result=[]
    print("from orchestrator files = ")
    for filename,lines in files:
        print("checking each file wordcount")
        mapoutput = yield context.call_activity("E1_Map", lines)
        # Shuffle phase
        shuffleddata = yield context.call_activity("E1_Shuffle", mapoutput)

        # Reduce phase
        wordcounts = yield context.call_activity("E1_Reduce", shuffleddata)
        result.append((filename,wordcounts))

    return result

main = df.Orchestrator.create(hello_orchestrator)
