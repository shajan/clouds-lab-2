def main(lines):
    map_outputs = []
    for line_number, line in lines:
        words = line.split()
        for word in words:
            map_outputs.append((word, 1))
    return map_outputs