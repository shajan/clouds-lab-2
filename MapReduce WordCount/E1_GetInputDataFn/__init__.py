import os, uuid
from azure.identity import DefaultAzureCredential
from azure.storage.blob import BlobServiceClient, BlobClient, ContainerClient


def main(connectstr):
    try:
        # Create the BlobServiceClient object
        blob_service_client = BlobServiceClient.from_connection_string(connectstr)
        # Quickstart code goes here
        container_name="mrinputs"
        local_path = os.path.join(os.environ['HOME'], 'data')
        if not os.path.exists(local_path):
            os.mkdir(local_path)
       
        container_client = blob_service_client.get_container_client(container= container_name) 
        files=[]
        blob_list = container_client.list_blobs()
        for blob in blob_list:
            print("\t" + blob.name)
            lines=[]
            local_file_name = blob.name + ".txt"
            download_file_path = os.path.join(local_path, str.replace(local_file_name ,'.txt', 'DOWNLOAD.txt'))
            with open(file=download_file_path, mode="wb") as download_file:
                download_file.write(container_client.download_blob(blob.name).readall())
            
            with open(file=download_file_path, mode="r", encoding="utf-8") as download_file:
                for line_number, line in enumerate(download_file, start=1):
                    lines.append((line_number, line.strip()))
            files.append((blob.name,lines))
        return files

    except Exception as ex:
        print('Exception:')
        print(ex)
        return []