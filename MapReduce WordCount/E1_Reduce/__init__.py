def main(shuffleddata):
    print()
    word_counts = []
    for key, values in shuffleddata.items():
        total_count = sum(values)
        word_counts.append((key, total_count))
    return word_counts
