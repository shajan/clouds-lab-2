from flask import Flask, request, jsonify
import math
app = Flask(__name__)

def integrate_abs_sin(lower, upper, N):
    delta_x = (upper - lower) / N
    integral_sum = 0.0

    for i in range(N):
        x_i = lower + i * delta_x
        integral_sum += abs(math.sin(x_i)) * delta_x

    return integral_sum

@app.route('/numericalintegralservice/<float:lower>/<float:upper>', methods=['GET'])
def numerical_integral_service(lower, upper):
    N_values = [10, 100, 1000, 10000, 100000, 1000000]
    results={}

    for N in N_values:
        result = integrate_abs_sin(lower, upper, N)
        results[N] = result
    return jsonify(results)

if __name__ == "__main__":
    app.run(debug=True)
