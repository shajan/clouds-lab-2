import azure.functions as func
import logging
import math

app = func.FunctionApp(http_auth_level=func.AuthLevel.ANONYMOUS)

def integrate_abs_sin(lower, upper, N):
    delta_x = (upper - lower) / N
    integral_sum = 0.0

    for i in range(N):
        x_i = lower + i * delta_x
        integral_sum += abs(math.sin(x_i)) * delta_x

    return integral_sum


@app.route(route="integral", methods=['GET'])
def integral(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    lower = float(req.params.get('lower'))
    upper = float(req.params.get('upper'))

    N_values = [10, 100, 1000, 10000, 100000, 1000000]
    results={}

    for N in N_values:
        result = integrate_abs_sin(lower, upper, N)
        results[N] = result
    # results = integrate_abs_sin(lower, upper, 1000)  # You can choose an appropriate N value here

    return func.HttpResponse(f"Result: {results}", status_code=200)
    # name = req.params.get('name')
    # if not name:
    #     try:
    #         req_body = req.get_json()
    #     except ValueError:
    #         pass
    #     else:
    #         name = req_body.get('name')

    # if name:
    #     return func.HttpResponse(f"Hello, {name}. This HTTP triggered function executed successfully.")
    # else:
    #     return func.HttpResponse(
    #          "This HTTP triggered function executed successfully. Pass a name in the query string or in the request body for a personalized response.",
    #          status_code=200
    #     )